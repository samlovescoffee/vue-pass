import Vue from 'vue'
import Router from 'vue-router'
import signUp from './signUp'
import search from './search'
import account from './account'
import create from './create'
import viewPost from './viewPost'
import postArchive from './postArchive'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'signUp',
      component: signUp
    },
    {
      path: '/search',
      name: 'search',
      component: search
    },
    {
      path: '/user/:id',
      name: 'account',
      component: account
    },
    {
      path: '/create',
      name: 'create',
      component: create,
    },
    {
      path: '/posts/:name',
      name: 'viewPost',
      component: viewPost,
    },
    {
      path: '/posts/',
      name: 'postArchive',
      component: postArchive,
    }
  ]
})
