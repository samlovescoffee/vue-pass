# vue-pass

> A node, express, mongo project. Designed as a headless CMS, I've slapped a Vue.js front end in the mix for testing. 

## Goals

1. Security through JSON web tokens.
2. Simple post management/creation.
3. Non intrusive admin functionality.

## Build Setup

``` bash
# install dependencies
npm install - currently need to install some dependencies manually, check the package.json

# serve with hot reload at localhost:8080
npm run start

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```