const mongoose = require('mongoose');
const tracking = require('../model/tracking');
const MongooseTracking = mongoose.model('Record','records');
const log = require('./logs');

const tracker = {
    send: function(post) {
        return new Promise ((resolve, reject) => {
            MongooseTracking.update(
                {"postId": post},
                {"postId": post, "time": new Date()},
                { upsert : true }
            )
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            });
        });
    },
    get: function(post) {
        return new Promise ((resolve, reject) => {
            MongooseTracking.find({ "id": post })
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        });
    }
}

module.exports = tracker;