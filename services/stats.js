const mongoose = require('mongoose');

// may not need the models
// const tracking = require('../model/tracking');
// const users = require('../model/users');
// const content = require('../model/content');
// const comments = require('../model/comments');
// const logs = require('../model/logs');

const MongooseTracking = mongoose.model('Record','records');
const MongooseUsers = mongoose.model('User','users');
const MongooseContent = mongoose.model('Post','posts');
const MongooseComments = mongoose.model('Comment','comments');
//const MongooseErrors = mongoose.model('Error','error'); Schema error, because it's a schema object?

const log = require('./logs');
const options = {
    tracking: MongooseTracking,
    users: MongooseUsers,
    posts: MongooseContent,
    comments: MongooseComments
}

const stats = {
    get: function(params) {
        return new Promise ((resolve, reject) => {
            options[params.collection].count({})
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        });
    }
}

module.exports = stats;