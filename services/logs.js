const logs = require('../model/logs');

const log = {
	audit: function dbLog(message, email) {
		let log = new logs.audit;
		log.Email = email;
		log.Message = message;
		log.Date = new Date();

		log.save()
		.then(function(val){
			resolve(val);
		})
		.catch(function(err){
			log.error(err);
			reject(err);
		})
	},

	error: function dbError(err, email) {
		let log = new logs.error;
		log.Error = err;
		log.Email = email,
		log.Date = new Date();

		log.save()
		.then(function(val){
			resolve(val);
		})
		.catch(function(err){
			log.error(err);
			reject(err);
		})
	}
};

module.exports = log;
