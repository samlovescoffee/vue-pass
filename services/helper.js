const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const MongooseUsers = mongoose.model('User','users');
const log = require('./logs');

const helper = {
    validatePassword: function checkPasswordRegex(password) {
			let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
			return re.test(password);
	},
	validateEmail: function checkPasswordRegex(email) {
			let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
	},
	slugify: function(string) {
		return string.toString().toLowerCase().replace(/[^a-zA-Z ]/g, "").replace(/ /g,"_");
	},
	getLoggedInUser: function(req) {
		let payload = helper.parseJWT(req.headers.authorization);
		return new Promise ((resolve, reject) => {
			MongooseUsers.find({ "_id": payload.id })
			.then(function(val){
				resolve(val)
			})
			.catch(function(err){
				log.error(err);
				reject(err);
			})
        });
	},
	parseJWT: function(token) {
		return jwt.verify(token.substring(7), 'test');
	}
}

module.exports = helper;