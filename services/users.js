const User = require('../model/users');
const passwordHash = require('password-hash');
const log = require('./logs');
const helper = require('./helper');
const mongoose = require('mongoose');
const MongooseUsers = mongoose.model('User','users');
const jwt = require('jsonwebtoken');

const user = {
	signUpCheck: function(req) {
		if(req.body.signUp === "true") {
			return true;
		} else {
			return false;
		}
	},
	create: function createUser(req, res) {
		return new Promise ((resolve, reject) => {
			user.find('Email', req.body.email)
			.then(function(val, res){
				if (val.length === 0 && helper.validatePassword(req.body.password) && helper.validateEmail(req.body.email)) {
					let newUser = new User();
					newUser.Email = req.body.email;
					newUser.Password = passwordHash.generate(req.body.password);
					newUser.CreatedDate = new Date();
					newUser.Username = req.body.username;

					if (!['sam@intravenous.coffee', 'elise_t92@hotmail.com'].includes(newUser.Email)) {
						newUser.Access = 'User';
					} else {
						newUser.Access = 'Admin';
					}

					newUser.save()
					.then(function(){
						log.audit('Successful sign up', req.body.email);
						let creationSuccess = user.find('Email', req.body.email)
						resolve(creationSuccess);
					})
					.catch(function(err){
						log.error(err);
						reject(err);
					});
				} else {
					reject('User with this email may already exist');
				}
			})
			.catch(function(error){
				log.error(error);
				reject(err);
			});
		});
	},
	find: function finder(col, searchTerm, res) {
		return new Promise ((resolve, reject) => {
			MongooseUsers.find({ [col]: searchTerm })
			.then(function(val) {
				resolve(val);
			})
			.catch(function(err){
				log.error(err);
				reject(err);
			})
		});
	},
	validate: function handleSubmission(req, res) {
		return new Promise ((resolve, reject) => {
			user.find('Email', req.body.email)
			.then(function(val){
				if (val.length === 0) {
					resolve('User does not exist');
				} else {
					if (passwordHash.verify(req.body.password, val[0].Password)) {
						log.audit('Successful log in request', req.body.email);
						resolve(val);
					} else {
						log.audit('Unsuccessful log in', req.body.email);
						resolve('Unsuccessful log in')
					}
				}
			})
			.catch(function(err){
				log.error(err);
				reject(err);
			});
		});
	},
	checkForJwt: function(val) {
		if ('id' in val[0]) {
			let token = jwt.sign({ "username": val[0].Username, "access": val[0].Access, "id": val[0].id }, 'test');
			let item = {
				"JWT": token
			}
			return item;
			
		} else {
			res.status(401).send(val);
		}
	}
	
};
module.exports = user;