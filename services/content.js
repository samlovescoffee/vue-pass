const Content = require('../model/content');
const log = require('./logs');
const helper = require('./helper');
const mongoose = require('mongoose');
const MongoosePosts = mongoose.model('Post','posts');

const content = {
    create: function(data, user) {
        return new Promise ((resolve, reject) => {
            content.commit(data, user)
            .then(function(res) {
                resolve(res);
            })
            .catch(function(err) {
                log.error(err);
                reject(err);
            })
        });
    },
    commit: function(data, user) {
        return new Promise ((resolve, reject) => {
            let newContent = new Content.post();
            newContent.date = new Date();
            newContent.title = data.title;
            newContent.data = data.content;
            newContent.slug = helper.slugify(data.title);
            newContent.author = user[0].id;
            content.findBySlug(newContent.slug)
                .then(function(res) {
                    if(res.length > 0) {
                        let appendage = Date.now().toString();
                        newContent.slug = newContent.slug + '_' + appendage;
                    }
                    newContent.save()
                    .then(function(val){
                        resolve(val);
                    })
                    .catch(function(err){
                        log.error(err);
                        reject(err);
                    })
                })
                .catch(function(err) {
                    log.error(err);
                    reject(err);
                })
        });
    },
    findBySlug: function(slug) {
        return new Promise ((resolve, reject) => {
            MongoosePosts.find({ "slug": slug })
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        });
    },
    getBulk: function() {
        return new Promise ((resolve, reject) => {
            MongoosePosts.find({})
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        });
    }
}

module.exports = content;
