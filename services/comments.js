const Comment = require('../model/comment');
const log = require('./logs');
const helper = require('./helper');
const mongoose = require('mongoose');
const MongooseComments = mongoose.model('Comment','comments');

const comment = {
    create: function(data, user) {
        return new Promise ((resolve, reject) => {
            comment.commit(data, user)
            .then(function(res) {
                resolve(res);
            })
            .catch(function(err) {
                log.error(err);
                reject(err);
            })
        });
    },
    commit: function(data, user) {
        return new Promise ((resolve, reject) => {
            let newComment = new Comment.post();
            newComment.date = new Date();
            newComment.post = data.post;
            newComment.author = user[0].Username;
            newComment.text = data.text;

            newComment.save()
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        })
    },
    bulkGet: function(post) {
        return new Promise ((resolve, reject) => {
            MongooseComments.find({ "post" : post })
            .then(function(val){
                resolve(val);
            })
            .catch(function(err){
                log.error(err);
                reject(err);
            })
        })
    }
}

module.exports = comment;