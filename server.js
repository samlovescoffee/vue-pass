'use strict';
const express = require('express'),
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  app = express(),
  router = express.Router({mergeParams: true}),
  user = require('./services/users'),
  log = require('./services/logs'),
  helper = require('./services/helper'),
  content = require('./services/content'),
  comment = require('./services/comments'),
  tracking = require('./services/tracking'),
  stats = require('./services/stats'),
  cors = require('cors'),
  jwt = require('jsonwebtoken'),
  expressJwt = require('express-jwt'),
  redisClient = require('redis').createClient(),
  session = require('express-session'),
  RedisStore = require('connect-redis')(session),
  cookieParser = require('cookie-parser');

mongoose.Promise = global.Promise;

// change this to your db
mongoose.connect('mongodb://mongoLab:thisIsATest@ds137054.mlab.com:37054/user-pass'), {
	useMongoClient: true
};

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
	session({
		secret: 'test',
		store: new RedisStore({
			client: redisClient
		})
	}
));

app.options('*', cors());

// middle ware
app.use(expressJwt({ secret: 'test' }).unless({ path: [
	'/api/users',
	/\/api\/contentView/
] }));

router.route('/users')
.post(function(req, res) {
	if (user.signUpCheck(req)) {
		user.create(req, res)
		.then(function(val){
			res.status(200).send(user.checkForJwt(val));
		})
		.catch(function(err){
			log.error(err);
			// This is horrible - try harder
			if (err === 'User with this Email may already exist') {
				res.status(405).send(err);
			} else {
				res.status(500).send('Internal server error');
			}

		})
	} else {
		user.validate(req, res)
		.then(function(val){
			res.status(200).send(user.checkForJwt(val));
		})
		.catch(function(err){
			log.error(err);
			res.status(500).send('Internal Server Error');
		});
	}
});

router.route('/userSearch')
.post(function(req, res) {
	user.find("Username", req.body.searchTerm, res)
	.then(function(val){
		res.send(val);
	})
	.catch(function(err){
		log.error(err);
		res.status(500).send('Internal Server Error');
	})
});

router.route('/contentView/:post')
.get(function(req, res) {
	content.findBySlug(req.params.post)
	.then(function(val) {
		res.status(200).send(val[0]);
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send('Internal Server Error');
	})
});

router.route('/bulkPosts/')
.get(function(req, res) {
	content.getBulk()
	.then(function(val) {
		res.send(val);
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send('Internal Server Error');
	})
});

router.route('/contentCreate')
.post(function(req, res) {
	helper.getLoggedInUser(req)
	.then(function(val){
		content.create(req.body, val)
		.then(function(val){
			res.send(val);
		})
		.catch(function(err){
			log.error(err);
			res.status(500).send('User Management Error');
		})
	})
	.catch(function(err){
		log.error(err);
		res.status(500).send('Content Service Error');
	})

});

router.route('/commentCreate')
.post(function(req,res) {
	helper.getLoggedInUser(req)
	.then(function(val){
		comment.create(req.body, val)
		.then(function(val){
			res.send(val);
		})
		.catch(function(err){
			log.error(err);
			res.status(500).send('User Management Error');
		})
	})
	.catch(function(err){
		log.error(err);
		res.status(500).send('Comment Service Error');
	})
});

router.route('/comments/:post')
.get(function(req, res) {
	comment.bulkGet(req.params.post)
	.then(function(val){
		res.send(val);
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send('Post Error');
	})
});

router.route('/tracking/:post')
.post(function(req, res){
	tracking.send(req.params.post)
	.then(function(val){
		res.send(val);
	})
	.catch(function(){
		log.error(err);
		res.status(500).send('Tracking Issue')
	});
});

router.route('/countRecords/:collection')
.get(function(req, res){
	stats.get(req.params)
	.then(function(val){
		res.status(200).send({data: val});
	})
	.catch(function(err){
		log.error(err);
		res.status(500);
	})
})

app.use('/api', cors(), router);

app.listen(3001, function() {
	console.log('api running');
});
