'use strict';
//import dependency
const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
//create new instance of the mongoose.schema. the schema takes an
//object that shows the shape of your database entries.

let content = {
	post: mongoose.model('Post', new Schema({
		data: {},
		author: String,
		date: Date,
		title: String,
		slug: String
	}))
};

//export our module to use in server.js
module.exports = content;
